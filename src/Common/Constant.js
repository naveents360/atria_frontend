const endpoints = {
    addSensorData : "addsensordata",
    getSensorData : "getsensordata",
    getSensorDataBySensor : "getsensordatabysensor",
    getSensorDataByDate : "getsensordatabydate" ,
    addSensorType : "addsensortype",
    getSensorType : "getsensortype",
    getSensorDataByTime:"getdatabytime"
}

const API_URL ="https://localhost:8000";

export {API_URL,endpoints};
