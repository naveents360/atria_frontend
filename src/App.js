import React from 'react';
import { Redirect } from  'react-router-dom';
import Home from './Components/Home'
import AddSensorType from './Components/AddSensorType'
import AddSensorData from './Components/AddSensorData'
import SensorTypeLinks from './Components/SensorTypeLinks'
import DataVisualizer from './Components/DataVisualizer'
import Chartexample from './Components/Chartexample'
import { BrowserRouter as Router, Route,Switch } from 'react-router-dom';

class App extends React.Component{
  render(){
  return(
    <Router>
    <Switch> 
    <Route exact path='/home' component={Home}></Route>
    <Route exact path='/addsensortype' component={AddSensorType}></Route>
    <Route exact path='/addsensordata' component={AddSensorData}></Route>
    <Route exact path='/sensortypedata' component={SensorTypeLinks}></Route>
    <Route exact path='/datavisulizer' component={DataVisualizer}></Route>
    <Route exact path='/chartexample' component={Chartexample}></Route>
    <Route render={() => <Redirect to="/home" />} />
      </Switch> 
      </Router>
    )
  }
}
  
export default App;