import axios from 'axios';

const loginAxios = axios.create();

loginAxios.defaults.headers.common["Accept"] = "application/json";

export { loginAxios };
