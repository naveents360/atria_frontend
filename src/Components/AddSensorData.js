import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL,endpoints } from './../Common/Constant';
import { Redirect } from  'react-router-dom';



class AddSensorData extends React.Component{
    constructor(props){
        super(props);
        this.state={
        sensortype:'',
        reading:'',
        open:false,
        loading:false,
        sensors:[],
        return:false
        }     
       }
       componentDidMount(){
        axios.get(API_URL+'/'+endpoints.getSensorType,)
 .then(response => {
     this.setState({sensors:response.data.data.sort()})
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }


       handleClose = event =>{
         event.preventDefault();
         this.setState({open:!this.state.open})
       }
       handlereading = event =>{
        event.preventDefault();
        this.setState({reading: event.target.value})
    }
       handleChangesensortype = event =>{
           event.preventDefault();
           this.setState({sensortype: event.target.value})
       }
      
        handleClick(event){
        event.preventDefault();
            const payload = {
               "reading":this.state.reading,
               "sensorType":this.state.sensortype.toLowerCase()
           }           
          this.setState({loading: !this.state.loading})
        this.setState({open: !this.state.open})
        this.setState({message:''})
        axios.post(API_URL+'/'+endpoints.addSensorData, payload)
        .then(res => {
          this.setState({loading:!this.state.loading})
            if(res.status === 200){
            this.setState({message:res.data.message})           
          }
          else if(res.status === 201){
             
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){            
            this.setState({return:!this.state.return})
        }
          })     
          this.setState({reading:'',sensortype:''})
        }       
        
    render(){
      const disabled = !this.state.reading.length || !this.state.sensortype.length;
      if(this.state.return){
        return <Redirect to="/home"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Onboard Sensor Data
        </Typography>
        <form  noValidate>
          <TextField           
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Reading Value"
            name="reading"      
            autoFocus
            onChange = {this.handlereading}
            helperText = {this.state.iphelptext}  
            value={this.state.reading}          
          />
          <br/>
    <FormControl variant="outlined" margin="normal" required
          fullWidth>
        <InputLabel>sensorType</InputLabel>
        <Select          
          value={this.state.sensortype}
          onChange={this.handleChangesensortype}
          label="Project"
        >
          {
            this.state.sensors.map((sensortype,index)=>
            <MenuItem key={index} value={sensortype}>{sensortype.charAt(0).toUpperCase() + sensortype.slice(1)}</MenuItem>)
          }
        </Select>
      </FormControl>  
          
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Submit            
           </Button>                
            <br/>        

            
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default AddSensorData;