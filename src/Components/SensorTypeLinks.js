import { Redirect,Link } from  'react-router-dom';
import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {API_URL,endpoints } from './../Common/Constant';
import axios from 'axios';


class SensorTypeLinks extends React.Component{
    constructor(props){
        super(props);
        this.state={
            sensors:[],
            return:false
        }
       }

       componentDidMount(){
        axios.get(API_URL+'/'+endpoints.getSensorType,)
 .then(response => {
     this.setState({sensors:response.data.data.sort()})
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });
    }

    render(){
        if(this.state.return){
            return <Redirect to="/home"/>
        }
      return(
        <Container component="main" maxWidth="xs"style={{textAlign:"center"}}>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Sensor types List
            </Typography>
            {
            this.state.sensors.map((sensortype,index)=>
            <div key={index}>
                <Button>
            <Link to={{pathname:"/datavisulizer",  state:{
                sensortype : {sensortype},
            }}}>{sensortype.charAt(0).toUpperCase() + sensortype.slice(1)}</Link></Button><br/></div> 
            )
          }
         </Container>
   )
    }
  }
    
export default SensorTypeLinks;