import React from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from  'react-router-dom';



class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
        }
       }

    render(){
        return(            
            <Container component="main" maxWidth="xs"style={{textAlign:"center"}}>
            <CssBaseline />
            <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
                   Home 
            </Typography>
            <Button><Link to="/addsensortype">ADD Sensor Type</Link></Button>
            <br />
            <Button><Link to="/addsensordata">ADD Sensor Data</Link></Button>
            <br />
            <Button><Link to="/sensortypedata">Visualize Sensor Data</Link></Button>
            </Container>
        )
      }
}

export default Home;