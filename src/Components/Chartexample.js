import React from 'react';
import {Line} from 'react-chartjs-2';
import dateconv from './../Common/Utils'

// var state = {
//   labels: ['Mon Feb 08 2021 15:20:52', 'Mon Feb 08 2021 15:20:52', 'Mon Feb 08 2021 15:20:52'],
//   datasets: [
//     {
//       label: 'Sensor Statistics',
//       backgroundColor: 'rgba(75,192,192,1)',
//       borderColor: 'rgba(0,0,0,1)',
//       borderWidth: 2,
//       data: [25, 36, 0.6]
//     }
//   ]
// }

export default class Chartexample extends React.Component {
    constructor(props){
        super(props);
        this.state={
          labels:  [],
          data: [],
          overalldata:{}
        }
    }
    componentDidMount() {
        this.setState({overalldata : this.props.location.state.data.items})
        let sensors = this.props.location.state.data.items;
        for(var i=0;i< sensors.length;i++){
            this.state.data[i] = sensors[i].reading
            this.state.labels[i] = (sensors[i].timestamp)
        }
        var state = {
            labels: this.state.labels,
            datasets: [
              {
                label: 'Sensor Statistics',
                backgroundColor: 'rgba(75,192,192,1)',
                borderColor: 'rgba(0,0,0,1)',
                borderWidth: 2,
                data: this.state.data
              }
            ]
          }

          this.setState({overalldata:state})
        //this.setState({labels : this.props.location.state.labels})
        //this.setState({data : this.props.location.state.data})
    }


  render() {
    return (
      <div>
        <Line
          data={this.state.overalldata}
          options={{
            title:{
              display:true,
              text:'Sensor statistics',
              fontSize:20
            },
            legend:{
              display:true,
              position:'right'
            }
          }}
        />
      </div>
    );
  }
}