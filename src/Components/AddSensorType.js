import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL,endpoints } from './../Common/Constant';
import { Redirect } from  'react-router-dom';



class AddSensorType extends React.Component{
    constructor(props){
        super(props);
        this.state={
        sensortype:'',
        open:false,
        loading:false,
        return:false,
        message:''
        }     
       }

       handleClose = event =>{
        event.preventDefault();
        this.setState({open:!this.state.open})
      }

       handleChangesensortype= event =>{
           event.preventDefault();
           this.setState({sensortype:event.target.value})
       }
      
        handleClick(event){
        event.preventDefault();
            const payload = {
               "sensortype":this.state.sensortype,
           }          
        this.setState({loading: !this.state.loading})
        this.setState({open: !this.state.open})
        this.setState({message:''})
        // const options = {
        //     headers: {
        //         'Access-Control-Allow-Origin': '*',
        //         'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        //     }
        //   };
        axios.post(API_URL+'/'+endpoints.addSensorType,  payload)
        .then(res => {
          this.setState({loading:!this.state.loading})
            if(res.status === 200){
            this.setState({message:res.data.message})           
          }
          else if(res.status === 201){
             
              this.setState({message:res.data.message})
          }
        })
        .catch(error => {
          if(error.response){            
            this.setState({return:!this.state.return})
        }
          })     
          this.setState({sensortype:''})
        }
        

         
        
    render(){
      const disabled = !this.state.sensortype.length;
      if(this.state.return){
        return <Redirect to="/signin"/>
    }
    return(
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               Onboard sensortype
        </Typography>
        <form  noValidate>
          <TextField           
            variant="outlined"
            margin="normal"
            fullWidth
            required
            label="Sensor Type"
            name="sensortype"      
            autoFocus
            onChange = {this.handleChangesensortype}  
            value={this.state.sensortype}          
          />
          <br/>          
          <Button
            disabled = {disabled}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={(event) => this.handleClick(event)}
            >
            Submit            
           </Button>                           
        <Dialog          
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {this.state.loading &&  <CircularProgress />}
            {this.state.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
         { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
             OK
            </Button>}
          </DialogActions>
        </Dialog>

        </form>
        </Container>
    )

}
}

export default AddSensorType;