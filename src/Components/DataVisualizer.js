import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { Link } from  'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import {API_URL,endpoints } from './../Common/Constant';
import {dateconv} from './../Common/Utils'
import {Redirect } from  'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';



class DataVisualizer extends React.Component {
  constructor(props){
    super(props);
    this.state={
      items:  [],
      isloaded: false,
      open:false,
      min:'',
      max:'',
      total:'',
      mean:'',
      sensortype : '',
      loading:false,
      message:'',
      return:false,
      time:''
  }
  }

    
  handleClose = event  =>{
    event.preventDefault();
    this.setState({open:!this.state.open})
  }
  handleEnv = event =>{
    event.preventDefault();
    this.setState({time: event.target.value})
    this.setState({open:!this.state.open})
    this.setState({loading:!this.state.loading})
    let ab  = event.target.value
    console.log(ab)
    axios.get(API_URL+'/'+endpoints.getSensorDataByTime+'?sensortype='+this.props.location.state.sensortype.sensortype+'&seconds='+ab)
 .then(res => {
     this.setState({loading:!this.state.loading})
     this.setState({open:!this.state.open})
     if(res.status === 200){
     this.setState({min:res.data.min})
     this.setState({max:res.data.max})
     this.setState({total:res.data.total})
     this.setState({mean:res.data.mean})
     this.setState({items:res.data.sensordata})
     }
     else if(res.status === 201){
      this.setState({items:''})
      this.setState({message:res.data.msg})
  }
  })
 .catch((error) => {
  if(error.response){
    this.setState({return:!this.state.return})
  }
  });

}

    componentDidMount() {
      this.setState({loading:!this.state.loading})
      this.setState({open: !this.state.open })
      this.setState({sensortype : this.props.location.state.sensortype})
      axios.get(API_URL+'/'+endpoints.getSensorDataBySensor+'/'+this.props.location.state.sensortype.sensortype)
        .then(res => {
          this.setState({loading:!this.state.loading})          
          if(res.status === 200){

        this.setState({items:res.data.sensordata})
        this.setState({min:res.data.min})
        this.setState({max:res.data.max})
        this.setState({total:res.data.total})
        this.setState({mean:res.data.mean})
        this.setState({open: !this.state.open })            
        }
              })
      .catch(error => {
        if(error.response){
          this.setState({return:!this.state.return})
      }
        })     
    
    }
  render() {
    if(this.state.return){
      return <Redirect to="/home"/>
  }
    var {items}=this.state;
    return (
      
      <TableContainer>
        <CssBaseline />
        <Typography component="h1" variant="h5" style={{textAlign:"center"}}> 
               {this.state.ip}  
        </Typography>
          <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            required
            label="Minimum value"
            name="min"    
            value={this.state.min}          
          />
          <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            required
            label="Maximum Value"
            name="max"    
            value={this.state.max}          
          />
          <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            required
            label="Total"
            name="total"    
            value={this.state.total}          
          />
           <TextField           
            disabled 
            variant="outlined"
            margin="normal"
            required
            label="Avg value"
            name="mean"    
            value={this.state.mean}          
          />
          <br/>
            <Button 
            variant="contained"
            color="primary">
            <Link to={{pathname:"/chartexample",  state:{
                data : {items},
            }}}>Line Chart</Link></Button>
              <FormControl variant="outlined" margin="0" 
              maxWidth="xs"
              required>
        <InputLabel>Time</InputLabel>
        <Select          
          value={this.state.time}
          onChange={this.handleEnv}
          label="Time"
        >
          <MenuItem value="300">Last 5 Min</MenuItem>
          <MenuItem value="900">Last 15 Min</MenuItem>
          <MenuItem value="1800">Last Half an Hour</MenuItem>
          <MenuItem value="3600">Last one Hour</MenuItem>
          <MenuItem value="10800">Last 3 Hours</MenuItem>
          <MenuItem value="21600">Last 6 Hours</MenuItem>
          <MenuItem value="43200">Last 12 Hours</MenuItem>
          <MenuItem value="86400">Last 24 Hours</MenuItem>
          {/* <MenuItem value="REPLICA">REPLICA</MenuItem>     */}
        </Select>
      </FormControl>
          <br/>           
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>Sl.No</TableCell>
              <TableCell align="center">Readings&nbsp;</TableCell>
              <TableCell align="center">SensorType&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items
            .map((item,index) => (
              <TableRow key={index}>
                <TableCell component="th" scope="row">
                  {index+1}
                </TableCell>
                <TableCell align="center">{item.reading}</TableCell>
                <TableCell align="center">{item.sensorType}</TableCell>  
                {/* <TableCell align="center">{item.mount}</TableCell> */}             
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <Dialog          
       open={this.state.open}
       onClose={this.handleClose}
       aria-labelledby="responsive-dialog-title"
     >
       <DialogTitle id="responsive-dialog-title">{!this.state.loading && "Status"}</DialogTitle>
       <DialogContent>
         <DialogContentText>
         {this.state.loading &&  <CircularProgress/>}
         {this.state.message}
         </DialogContentText>
       </DialogContent>
       <DialogActions>
      { !this.state.loading &&<Button onClick={this.handleClose} color="primary" autoFocus>
          OK
         </Button>}
       </DialogActions>
     </Dialog>
      </TableContainer>      
    );
  }
 }

 export default DataVisualizer;